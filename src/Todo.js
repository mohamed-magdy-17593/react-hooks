import React, {useState} from 'react'
import Form from './Form'

function toggleCompolete({todos, setTodos, i: itemIndex}) {
  return e => {
    setTodos(
      todos.map(
        (t, i) => (i === itemIndex ? {...t, complete: !t.complete} : t),
      ),
    )
  }
}

function Todo() {
  const [todos, setTodos] = useState([])
  return (
    <div>
      <Form onSubmit={text => setTodos([{text, complete: false}, ...todos])} />
      <div>
        {todos.map((t, i) => (
          <div
            style={{
              textDecoration: t.complete ? 'line-through' : '',
            }}
            key={t.text}
            onClick={toggleCompolete({todos, setTodos, i})}
          >
            {t.text}
          </div>
        ))}
      </div>
    </div>
  )
}

export default Todo

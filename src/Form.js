import React, {useState} from 'react'

function useInputValue(initialValue) {
  const [value, setValue] = useState(initialValue)
  return {
    value,
    onChange: e => setValue(e.target.value),
    resetValue: _e => setValue(''),
  }
}

function Form({onSubmit}) {
  const {resetValue, ...text} = useInputValue('')
  return (
    <form
      action="post"
      onSubmit={e => {
        e.preventDefault()
        onSubmit(text.value)
        resetValue()
      }}
    >
      <input type="text" {...text} />
    </form>
  )
}

export default Form

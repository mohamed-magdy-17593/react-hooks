import React, {useState, useEffect} from 'react'

function L({children}) {
  return <pre>{JSON.stringify(children, null, 2)}</pre>
}

function Fetch() {
  const [users, setUsers] = useState(null)
  useEffect(() => {
    fetch('https://api.randomuser.me/')
      .then(res => res.json())
      .then(({results}) => setUsers(results))
  }, [])
  return <div>{users ? <L>{users}</L> : 'loading...'}</div>
}

function App() {
  return (
    <>
      <Fetch />
    </>
  )
}

export default App
